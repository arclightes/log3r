package com.arclightes.log3r;

interface BulkNumeralCharAppender extends NumeralCharAppender,
                                          CharBulkAppender,
                                          FlushableCharBuffer {

}
