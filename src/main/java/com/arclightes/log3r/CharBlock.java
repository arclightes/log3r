package com.arclightes.log3r;

import java.nio.CharBuffer;

public interface CharBlock {
    CharBuffer buffer();
    char[] array();
}
