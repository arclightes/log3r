package com.arclightes.log3r;

interface DoubleCharAppender {
    void appendDouble(double d);
    void appendDouble(double d, int precision);
}
