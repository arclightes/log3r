package com.arclightes.log3r;

import java.nio.CharBuffer;

// Not Thread Safe
class CharArrayLog3rMessage extends BaseLogMessage implements CharArrayMessage {
    private final BulkNumeralCharAppender integerBuffer = new NumeralCharArrayBuffer();
    private final FlushableDoubleCharAppender doubleBuffer = new DoubleCharArrayBuffer();
    private final TimestampCharAppender timestampAppender = new GregorianTimestampCharAppender(integerBuffer);

    CharArrayLog3rMessage() {
        super();
    }

    public final CharArrayMessage append(final LogMessage msg) {
        append(msg.array(), 0, msg.msgLength());
        return this;
    }

    public final CharArrayMessage append(final CharBlock block) {
        append(block.array());
        return this;
    }

    public final CharArrayMessage append(final char c) {
        array[msgLength++] = c;
        return this;
    }

    public final CharArrayMessage append(final char[] srcArray) {
        System.arraycopy(srcArray, 0, array, msgLength, srcArray.length);
        msgLength += srcArray.length;
        return this;
    }

    public final CharArrayMessage append(final char[] srcArray, final int srcPos, final int length) {
        System.arraycopy(srcArray, srcPos, array, msgLength, length);
        msgLength += length;
        return this;
    }

    public final CharArrayMessage append(final CharBuffer srcBuffer) {
        final int srcBufferLength = srcBuffer.length();
        if (srcBufferLength > 0) {
            srcBuffer.get(array, msgLength, srcBufferLength);
            msgLength += srcBufferLength;
        }

        return this;
    }

    public final CharArrayMessage append(final int i) {
        integerBuffer.appendInt(i);
        msgLength += integerBuffer.flush(array, msgLength);
        return this;
    }

    public final CharArrayMessage append(final long el) {
        integerBuffer.appendLong(el);
        msgLength += integerBuffer.flush(array, msgLength);
        return this;
    }

    public final CharArrayMessage append(final double d) {
        doubleBuffer.appendDouble(d);
        msgLength += doubleBuffer.flush(array, msgLength);
        return this;
    }

    public final CharArrayMessage append(final double d, final int precision) {
        doubleBuffer.appendDouble(d, precision);
        msgLength += doubleBuffer.flush(array, msgLength);
        return this;
    }

    public final CharArrayMessage appendMillisecondTimestamp(final long msTimestamp) {
        msgLength += timestampAppender.appendMillisecondTimestamp(msTimestamp, array, msgLength);
        return this;
    }
}

