package com.arclightes.log3r;

interface FlushableDoubleCharAppender extends DoubleCharAppender, FlushableCharBuffer {

}
