package com.arclightes.log3r;

import com.lmax.disruptor.*;
import com.lmax.disruptor.dsl.ProducerType;

import java.util.concurrent.Executor;

public interface LogContext {
    String contextName();
    int getFileLengthMb();
    int getMaxMessageLengthChars();
    int getMessagesQueueSize();
    int getFloatingPointPrecisionDefault();
    EventFactory<SequencedLogMessage> getLogMsgFactory();
    int getInputRingBufferSize();
    Executor getFileWriteExecutor();
    ProducerType getMessageEventProducerType();
    WaitStrategy getFileWriteWaitStrategy();
    EventHandler<SequencedLogMessage> getFileWriteEventHandler();
    LogTarget getDefaultLogTarget();
}