package com.arclightes.log3r;

public interface LogMessageType {
    LogMessage getNextMessage();
}
