package com.arclightes.log3r;

import junit.framework.Assert;
import org.junit.Test;

public class TestLog3rUtilsCorrectness {
    @Test
    public void testCharIntConversion() {
        char c = '1';
        int converted = (c - '0');
        Assert.assertEquals(converted, 1);
        c = (char) (c + 0);
        Assert.assertEquals(c, '1');
    }

    @Test
    public void testCharLongConversion() {
        char c = '1';
        long converted = c - '0';
        Assert.assertEquals(converted, 1L);
        c = (char) (c + 0);
        Assert.assertEquals(c, '1');
    }

    @Test
    public void printMaxValues() {
        System.out.println(Long.MAX_VALUE);
        final double maxDouble = Double.MAX_VALUE;
        System.out.println(Double.MAX_VALUE);
        System.out.println(Math.getExponent(maxDouble));
    }

    @Test
    public void printDoubleStrings() {
        for (int i = 0; i <= 32; i++) {
            System.out.println(i + "=" + Integer.toBinaryString(i) + ", ");
        }
    }

    @Test
    public void testInfinityRelationships() {
        Assert.assertEquals(Double.POSITIVE_INFINITY > 0, true);
        Assert.assertEquals(Double.NEGATIVE_INFINITY < 0, true);
    }
}
