package com.arclightes.log3r;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class TestNumeralAppendCorrectness {
    public TestNumeralAppendCorrectness() {

    }

    @Test
    public void testMinValueAppends() {
        NumeralCharArrayBuffer buffer = new NumeralCharArrayBuffer();

        buffer.appendInt(Integer.MIN_VALUE);
        char[] destArray = new char[100];
        buffer.flush(destArray, 0);
        char[] intMin = ("" + Integer.MIN_VALUE).toCharArray();
        char[] destSubArray = Arrays.copyOf(destArray, intMin.length);
        Assert.assertArrayEquals(intMin, destSubArray);

        buffer.appendLong(Long.MIN_VALUE);
        buffer.flush(destArray, 0);
        char[] longMin = ("" + Long.MIN_VALUE).toCharArray();
        char[] destSubArrayLong = Arrays.copyOf(destArray, longMin.length);
        Assert.assertArrayEquals(longMin, destSubArrayLong);
    }
}
